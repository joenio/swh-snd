# Papers to read

- Supporting Software History Exploration
- Modeling history to analyze software evolution
- Phenomenology of Program Maintenance and Evolution
- Life Cycles and Laws of Software Evolution
- Program Evolution, - Processes of Software Change
- Laws of Software Evolution Revisited
- Read SWH DAG de Merkle from API output as Gource format to render graph video (1st graph sonification experiment)
