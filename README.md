---
pagetitle: Software Heritage Sound
bibliography: swh-snd.bib
toc-title: swh-snd index
---

# Software Heritage Sound

**swh-snd** is an sound art experiment on top of Software Heritage archive.

> http://swh-snd.4two.art

## 1. Introduction

This project aims to investigate the software evolution, with special attention
to research software, throught the combination between data sonification and
data visualization with approach experimental (not as in scientific experiment)
and artistic.

The strategy of investigation takes inspiration from Paul Feyerabend:

> The only principle that does not inhibit progress is: anything goes
> [@feyerabend1975against].

## 2. Build

Requires [Pandoc][] 2.17.1.1 or later and [citeproc][].

```sh
make index.html
```

To build the `pdf` version.

```sh
make paper.pdf
```

Download [paper.pdf](paper.pdf)

## 3. Research Question

> Does data sonification combined with data visualization can enrich software
> evolution understanding?

> Can data sonification be used to understand research software datasets?

## 4. Background

### Software

TODO [@suber_what_1988].

### Research Software

TODO

> How does the rate of swhid adoption has been evolving?

#### Software Evolution



### Universal Software Archive

Software Heritage is ...  building the universal software archive, the project
collect and preserve software in source code form, the archive is
accessible ... because only by sharing it we can guarantee its preservation in the very long term

![](./software-heritage-id-grammar.png)

SWDID [@cosmo_2044_2018].

#### Software Heritage mirror

TODO: https://www.softwareheritage.org/mirrors/

### Research Software

TODO

> How does the rate of swhid adoption has been evolving?

### Sonification

TODO

Sonification isn't Data Sonification, sonification is the practical use of
sound to solve a question.

- https://en.wikipedia.org/wiki/Sonification

### Data Sonification

TODO [@kaper_data_2000].

### Software Sonification

TODO [@mancino_software_2017; @andrade_software_2014].

### Software Sonification tools

TODO

## 5. Data collection and analysis

_draft_: Fetch the source code of N versions for a software project X from SWH
Extract source code metrics for each version of each project, Or fetch source
code metrics from some API if available.

## 6. Technical References

- [Markdown With BibTeX References Using Zotero][glowkeeper]
- [Pandoc User’s Guide][pandoc-manual]

## Bibliography

[pandoc]: https://pandoc.org
[citeproc]: https://github.com/jgm/citeproc
[pandoc-manual]: https://pandoc.org/MANUAL.html?pandocs-markdown#citations
[glowkeeper]: https://glowkeeper.github.io/Markdown-with-References/
