PANDOC ?= pandoc

help:
	cat README.md

build: index.html paper.pdf

index.html: README.md
	$(PANDOC) --toc --toc-depth=3 --citeproc --standalone $< -o $@

paper.pdf: README.md
	$(PANDOC) --toc --toc-depth=3 --citeproc --standalone $< -o $@
